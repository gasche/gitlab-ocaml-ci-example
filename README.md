This repository contains a minimal OCaml dune project (a library with
a minimal test and minimal
[documentation](https://gasche.gitlab.io/gitlab-ocaml-ci-example)) as
an excuse to host a reusable [Gitlab CI
configuration](.gitlab-ci.yml).

The CI configuration assumes that 'dune' is used to build the project.

Features:

- The CI configuration is project-agnostic, so it should work
  unchanged for your own (simple) projects.

- It caches the opam dependencies instead of reinstalling them on each
  run.

- It builds the project, runs the tests and builds the documentation.
  (This can be tweaked by setting configuration variables.)

- Several compiler versions can be tested in parallel.

- It provides an easy way to upload content as “Gitlab project Pages”,
  for example documentation or release archives.

CI times are satisfying: on very small libraries I observe a 11mn job
time on the first run (or when cleaning the opam cache), and 2mn job
time on following runs.

The expected usage-mode of this CI configuration is that you copy it
in your own project. If you find that you need/want additional
features, ideally you would try to implement them in
a project-agonistic way and contribute them back to the example
repository.
