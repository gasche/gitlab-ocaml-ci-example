## development version

- use FF_USE_FASTZIP as a workaround to a Gitlab build that limits dune _build caching;
  see upstream issue https://gitlab.com/gitlab-org/gitlab-runner/-/issues/27496
  (Gabriel Scherer)
- Whether and how to build, test and doc is now configurable through variables
    DUNE_BUILD_TARGETS, DUNE_TEST_TARGETS, DUNE_DOC_TARGETS
  In particular making one of them empty will skip the corresponding step.
  (Gabriel Scherer, suggestion from Raphaël Proust)
- In the job log, show the URL of the generated documentatin in the artifacts.
  (Gabriel Scherer, suggestion from Raphaël Proust)
- Include _build/log in the artifacts.
  (Gabriel Scherer)
- Make dune-build flags configurable (Hugo Heuzard)

## 0.1

- Publish the sample project.
  (Gabriel Scherer)