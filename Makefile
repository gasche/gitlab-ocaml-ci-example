.PHONY: all
all:
	dune build @all

.PHONY: clean
clean:
	dune clean

.PHONY: test
test:
	dune runtest

.PHONY: doc
doc:
	dune build @doc

DOC_PATH := _build/default/_doc/_html

.PHONY: doc-browse
doc-browse: doc
	xdg-open ${DOC_PATH}/index.html

.PHONY: doc-commit-to-pages
doc-commit-to-pages: doc
# compute the current commit hash and branch name
	@git rev-parse --short HEAD > /tmp/commit
	@git rev-parse --abbrev-ref HEAD > /tmp/branch
	@git config branch.pages.remote > /tmp/remote-upstream-for-pages
	cp .gitlab-ci.yml /tmp

# move to the 'pages' branch and commit the documentation there
	git checkout pages
	mkdir -p docs
	@git rm --ignore-unmatch -r docs > /dev/null
	cp -r ${DOC_PATH} docs
	@git add docs
# we also update the 'pages' CI script to follow original-branch changes
	cp /tmp/.gitlab-ci.yml .
	git add .gitlab-ci.yml
	@git commit -m "documentation for $$(cat /tmp/branch) ($$(cat /tmp/commit))" \
	&& git checkout $$(cat /tmp/branch) \
	|| git checkout $$(cat /tmp/branch)

	@echo
	@echo "The documentation was committed to the 'pages' branch."
	@echo "You can now push it with"
	@echo "    git push $$(cat /tmp/remote-upstream-for-pages) pages"
